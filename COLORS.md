#dfe0df
#00c9ad
#ffcd8a
#a4692b
#008fa4
#402e32
#f2962f
#0a8756
#b9a89b
#524439
#a4692b
#c35354
#ff8886

<!-- https://youtu.be/-MZVhucxvao?t=99 -->

<!-- font-family: 'kg_happyregular'; -->
<!-- font-family: 'skrapbookregular'; -->

<!-- https://cssgradient.io/ -->


<!-- This is a list of CSS format specifiers and their respective outputs.

Specifier	Output 
 %s	 Formats the value as a string
 %i or %d	 Formats the value as an integer
 %f	 Formats the value as a floating point value
 %o	 Formats the value as an expandable DOM element. As seen in the Elements panel 
 %O	 Formats the value as an expandable JavaScript object
 %c	 Applies CSS style rules to the output string as specified by the second parameter -->


