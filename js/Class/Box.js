import { roundNumber, adaptiveFontSize } from '../tools/tools.js';
// import { roundNumber, adaptiveFontSize } from '../tools/tools.min.js';

/**
 * Draw a production box
 */
export default class Box {

    /**
     * Constructor of the box prodution.  
     * Call drawBox function to append it into the container.
     * @param {HTMLElements} container HTMLElement
     * @param {string} typeName name of the production chain
     * @param {number} worker base number of worker
     * @param {number} workerCost cost of a worker
     * @param {number} gainValue gained value for all worker
     * @param {string} icoUrl url of the image / icon
     */
    constructor(container, typeName, worker, workerCost, gainValue, icoUrl) {
        this.container = container
        this.typeName = typeName
        this.worker = worker
        this.workerCost = workerCost
        this.gainValue = gainValue
        this.icoUrl = icoUrl
    }

    /**
     * Draw the production box (construct and append)
     * @returns HTMLElement
     */
    drawBox() {
        // Box
        const div = document.createElement('div')
        div.id = '_box_production'

        // Box Layout
        const left = document.createElement('div')
        left.id = '_box_prod_left'
        const center = document.createElement('div')
        center.id = '_box_prod_center'
        const right = document.createElement('div')
        right.id = '_box_prod_right'

        // Icon & worker
        const ico = document.createElement('img')
        ico.id = '_box_prod_ico'
        ico.src = this.icoUrl
        const workers = document.createElement('div')
        workers.id = '_box_prod_worker'
        workers.textContent = this.worker


        // Center
        const type = document.createElement('div')
        type.textContent = this.typeName
        type.id = '_box_prod_name'
        type.style.fontSize = adaptiveFontSize(this.typeName)

        // Progress Bar 
        const barContent = document.createElement('div')
        barContent.className = 'progress-bar-container'
        const barProgress = document.createElement('div')
        barProgress.className = 'progress-bar'
        barProgress.id = '_bar_firm'
        const gainvalue = document.createElement('div')
        gainvalue.id = '_box_prod_gainvalue'
        gainvalue.textContent = this.gainValue

        // Right
        const price = document.createElement('div')
        price.id = '_box_prod_price'
        price.textContent = roundNumber(this.workerCost)

        const button = document.createElement('button')
        button.className = "ctrl-btn"
        button.id = "_box_buy_btn"
        button.textContent = "x0"

        const button2 = document.createElement('button')
        button2.id = "_box_speed_btn"
        button2.className = "ctrl-btn hidden"
        button2.textContent = "faster"

        // Append
        barContent.append(barProgress, gainvalue)
        left.append(ico, workers)
        center.append(type, barContent)
        right.append(price, button, button2)
        div.append(left, center, right)
        this.container.append(div)

        // return object of HTMLElements
        return {
            barProgress,
            workerButton: button,
            speedButton: button2,
            workers,
            gainvalue

        }
    }

}