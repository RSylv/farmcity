import { roundNumber } from '../tools/tools.js';
// import { roundNumber } from '../tools/tools.min.js';

export default class Society {

    constructor(birthTime, container) {
        this.person = 2
        this.ressource = 0
        this.birthTime = birthTime ? birthTime : 10000;
        this.birthNumber = 1
        this.birthQ = 3 // multiplier for birthNumber
        this.container = container
        this.gameContainer = document.createElement('div')
        this.gameContainer.id = '_container'
        this.gameContainer.className = 'home'
        this.gameBoxsContainer = document.createElement('div')
        this.gameBoxsContainer.id = '_boxs_container'
        // Sync Timer
        this.delay = this.birthTime
        this.timer = {}
    }

    birth() {
        let start = Date.now();

        // add person
        this.timer = setTimeout(() => {
            // add person
            this.person += this.birthNumber
            this.container.divPeopleValue.textContent = roundNumber(this.person)

            // calculate the actual number of ms since last time
            let actual = Date.now() - start;

            // subtract any extra ms from the delay for the next cycle
            this.delay = this.birthTime - (actual - this.birthTime);
            start = Date.now();

            // start the timer again
            this.birth();

        }, this.delay);

        // bar anime JS, with same delay
        document.getElementById('_bar_people').animate([
            { width: '1px' },
            { width: '100%' }
        ], {
            duration: this.delay,
            easing: 'linear',
            fill: 'both',
            iterations: 1
        });
    }

    drawHead() {
        // Head Game
        const headGame = document.createElement('div')
        headGame.id = '_head'

        // People
        const divPeople = document.createElement('div')
        divPeople.innerHTML = '<h3>People : </h3>'
        divPeople.id = '_people'
        const divPeopleValue = document.createElement('div')
        divPeopleValue.id = '_people_value'
        divPeopleValue.textContent = this.persons

        // Ressource
        const divRessource = document.createElement('div')
        divRessource.innerHTML = '<h3>Ressource : </h3>'
        divRessource.id = '_ressource'
        const divRessourceValue = document.createElement('div')
        divRessourceValue.id = '_ressource_value'
        divRessourceValue.textContent = this.ressources

        // Progress Bar 
        const barContent = document.createElement('div')
        barContent.className = 'progress-bar-container'
        const barProgress = document.createElement('div')
        barProgress.className = 'progress-bar'
        barProgress.id = '_bar_people'

        // Append
        barContent.append(barProgress)
        divPeople.append(divPeopleValue)
        divRessource.append(divRessourceValue)
        headGame.append(divPeople, divRessource)
        this.gameContainer.insertAdjacentElement("afterbegin", headGame)
        this.gameContainer.insertAdjacentElement("afterbegin", barContent)
        // this.gameContainer.append(barContent, headGame)

        return {
            divPeopleValue,
            divRessourceValue
        }
    }

    get persons() {
        return this.person;
    }
    set persons(number) {
        this.persons = number
    }

    get ressources() {
        return this.ressource;
    }
    set ressources(number) {
        this.ressource = number
    }

    set duration(number = 10000) {
        this.birthTime = number
        
        // reset timeout
        clearTimeout(this.timer)
        this.birth()
    }
    
}