import Production from './Production.js';
import Society from './Society.js';
import DATA from "../data.js";
import * as Tools from '../tools/tools.js';

// import Production from "./Production.min.js";
// import Society from "./Society.min.js";
// import DATA from "../data.min.js";
// import *as Tools from "../tools/tools.min.js";

/**
 * Class Firm (add production and unlock-btn)
 */
export default class Firm {

    /**
     * Firm constructor. 
     * @param {HTMLElement} container
     * @param {Society} society Society object
     * @param {string} domain Name / domain of the Firm
     */
    constructor(container, society, domain) {
        this.container = container
        this.society = society
        this.domain = domain.toUpperCase()
        this.productions = []
        this.worker = 0
        this.id = Tools.uniqueId()
        this.couldUpgrade = false
        this.upgradeCondition = 20 // base number of workers to upgrade
        this.upgradeQ = 2 // multiplier for upgradeCondition
        this.productionCount = 1
        // unlock Btn
        this.unlockButton = document.createElement('button')
        this.unlockButton.id = '_unlock_btn'
        this.unlockButton.className = 'desactivated'
        this.unlockButton.innerHTML = "Il faut " + this.upgradeCondition + " <span>" + (DATA[0].type[this.productionCount - 1].name).toUpperCase() + "S </span><br> pour débloquer <span>" + (DATA[0].type[this.productionCount].name).toUpperCase() + "</span> !! "
        this.unlockButton.addEventListener('click', () => { this.unlockButonAction() })
        // draw
        this.drawFirm()
    }

    /**
     * Construct and added a new production into the game.  
     * Push it into this.productions[] Array.  
     * @param {boolean} isFirst is this the first box ?  
     */
    async newProductions(isFirst = false) {
        // Get the model
        const model = await DATA[0].type[this.productionCount-1]

        // init workers set to One for the first box
        let worker = 0
        if (isFirst) { worker = 1 }

        // create production object
        const production = await new Production(
            this.society,
            this,
            model.name,
            model.cost,
            model.time,
            model.gain,
            model.ico,
            worker
        )       

        // push it into firm's production array
        this.productions.push(production)

        // increment counter
        this.productionCount++
    }

    /**
     * Unlock button, let it clickable to add new production.
     */
    unlockButonAction() {
        // if couldUpgrade is true
        if (this.couldUpgrade) {

            // For each unlock, speed the birth timer
            this.society.birthNumber *= this.society.birthQ
            // this.society.duration = this.society.birthTime - 5000

            // set new upgrade condition for the new Production, if it's not the first box
            this.upgradeCondition *= this.upgradeQ

            // if there are still types in the DATA array 
            if (DATA[0].type.length > this.productionCount) {

                // add new production and deactivate the button 
                this.newProductions()
                this.unlockButton.className = 'desactivated'
                
                // change button text 
                this.unlockButton.innerHTML = "Il faut " + this.upgradeCondition + " <span>" + (DATA[0].type[this.productionCount - 1].name).toUpperCase() + "S </span><br> pour débloquer <span>" + (DATA[0].type[this.productionCount].name).toUpperCase() + "</span> !! "
                
                // reinit upgrade value
                this.couldUpgrade = false

            } else {

                // add the last production and hide the button from display
                this.newProductions()
                this.unlockButton.classList.toggle('hidden', true)
                this.couldUpgrade = false
            }

        }
    }

    /**
     * Draw the Firm header (Domain name)
     */
    drawFirm() {
        // Firm layout
        const div = document.createElement('div')
        div.id = '_box_firm'

        // Domain label
        const domain = document.createElement('div')
        domain.textContent = "Domaine"
        domain.id = '_box_firm_domain'

        // Domain name
        const name = document.createElement('div')
        name.textContent = this.domain
        name.id = '_box_firm_name'

        // Append
        div.append(domain, name)
        const container = document.getElementById('_boxs_container')
        container.style.backgroundColor = "#008fa4"
        this.container.insertBefore(div, container)
    }

    get workers() {
        return this.worker;
    }
    set workers(number = 1) {
        this.worker += number
    }
    
}

