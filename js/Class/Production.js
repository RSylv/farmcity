import Box from './Box.js';
import Society from './Society.js';
import Firm from './Firm.js';
import DATA from "../data.js";
import { roundNumber } from '../tools/tools.js';

// import Box from './Box.min.js';
// import Society from './Society.min.js';
// import Firm from './Firm.min.js';
// import DATA from "../data.min.js";
// import { roundNumber } from '../tools/tools.min.js'; 


/**
 * Production Class.
 * Production chain of a firm.
 */
export default class Production {

    /**
     * Constructor of the Production Chain
     * @param {Society} society Society object.
     * @param {Firm} firm Firm object.
     * @param {string} type the type/name of the production 
     * @param {number} workerCost the cost of a worker
     * @param {number} productionTime the production time
     * @param {number} gainValue the gain value
     * @param {string} icoUrl the icon of the production chain
     * @param {number} worker number of worker
     */
    constructor(society, firm, type, workerCost, productionTime, gainValue, icoUrl, worker = 0) {
        this.container = society.gameBoxsContainer
        this.society = society
        this.firm = firm
        this.type = type.toUpperCase()
        this.worker = worker
        this.workerCost = workerCost
        this.productionTime = productionTime
        this.gain = gainValue
        this.icoUrl = icoUrl
        /**
         * Calculated the potential workers that we can buy
         * @returns {number} Number of workers
         */
        this.buy = () => {
            // calculate number of potential workers
            const potentialWorkers = Math.floor(this.society.ressources / this.workerCost)

            // if the result is greater than global person
            if (potentialWorkers > this.society.person) {
                this.box.workerButton.textContent = 'x' + roundNumber(this.society.persons);
                return this.society.persons
            } else {
                this.box.workerButton.textContent = 'x' + roundNumber(potentialWorkers);
                return potentialWorkers
            }
        }
        // controls variables
        this.speedCtrl = 10
        this.maxSpeed = 500
        this.isFixedAnimate = false
        this.fullSpeedUpgrade = false
        this.gainValue = gainValue * this.worker
        // draw the box. Contain html elements
        this.box = new Box(society.gameBoxsContainer, this.type, this.worker, this.workerCost, this.gainValue, this.icoUrl).drawBox()
        // listeners
        this.box.workerButton.addEventListener('click', () => {
            this.buyWorker();
            // unlock buttons access
            this.checkBtnsAccessibility()
        })
        this.box.speedButton.addEventListener('click', () => {
            this.addSpeed();
            // unlock buttons access
            this.checkBtnsAccessibility()
        })
        this.timer = {}
        // Sync Timer
        this.delay = this.productionTime
        // go work !
        this.work()
    }

    /**
     * setTimeout recursive function for the worker 
     */
    work() {
        // start time point to synchronize setTimeout
        let start = Date.now();

        // add ressources
        this.timer = setTimeout(() => {

            // add to ressources the value of work 
            this.society.ressource += this.gainValue
            this.society.container.divRessourceValue.textContent = roundNumber(this.society.ressource)

            // unlock buttons access and calcul potential worker
            this.checkBtnsAccessibility()
            this.buy()

            // calculate the actual number of ms since last time
            let actual = Date.now() - start;

            // subtract any extra ms from the delay for the next cycle
            this.delay = this.productionTime - (actual - this.productionTime);
            start = Date.now();

            // start the timer again
            this.work();

        }, this.delay);


        // ANIMATION
        if (!this.fullSpeedUpgrade) // If full speed upgrade is not reached
        {
            // progress-bar animation JS, with same delay
            this.box.barProgress.animate([
                { width: '1px' },
                { width: '100%' }
            ], {
                duration: this.delay,
                easing: 'linear',
                fill: 'both',
                iterations: 1
            });
        }
        else if (this.fullSpeedUpgrade && !this.isFixedAnimate) // If it is full speed upgrade, and the fixed animation has not start
        {
            this.fixedAnimation()
            // Fixed Animation is done
            this.isFixedAnimate = true;
        }
    }


    /**
     * Animation, if full speed upgrade is reached
     */
    fixedAnimation() {
        // progress-bar animation JS, with same delay
        this.box.barProgress.animate([
            { background: '#f2962f' },
            { background: '#f1c226', offset: .5 },
            { background: '#f2962f' }
        ], {
            duration: 4000,
            easing: 'ease-in-out',
            fill: 'both',
            iterations: Infinity
        })
    }


    /**
     * Buy Worker function
     */
    buyWorker() {
        const buying = this.buy()

        // if the number of people is greater than the requested purchase 
        if (buying <= this.society.persons) {

            // If enought ressources reached 
            if (0 <= this.society.ressources - this.workerCost) {

                // take away the cost from ressources
                this.society.ressources -= this.workerCost * buying

                // add worker
                this.worker += buying

                // update gainvalue data
                this.gainValue = this.gain * this.worker

                // update persons data
                this.society.person -= buying

                // update values on screen
                this.box.workers.textContent = roundNumber(this.worker)
                this.box.gainvalue.textContent = roundNumber(this.gainValue)
                this.society.container.divRessourceValue.textContent = roundNumber(this.society.ressources)
                this.society.container.divPeopleValue.textContent = roundNumber(this.society.persons)

                // reinit all btn to 0
                for (let i = 0; i < this.firm.productions.length; i++) {
                    this.firm.productions[i].box.workerButton.textContent = 'x0'
                }

                // if the number of workers is reached, show speed button 
                if (this.worker > this.speedCtrl && !this.fullSpeedUpgrade) {
                    this.box.speedButton.classList.toggle("hidden", false)
                }
            }

        }
    }


    /**
     * Add speed function
     */
    addSpeed() {
        // speed up production by 50%
        const speed = this.productionTime - (this.productionTime / 100) * 50

        // if the max speed is reached
        if (speed <= this.maxSpeed) {
            // block speed
            this.duration = this.maxSpeed
            // stop animation
            this.fullSpeedUpgrade = true
        } else {
            // increase the speed
            this.duration = speed
        }

        // hide speedButton
        this.box.speedButton.classList.toggle('hidden', true)

        // Increase speed ctrl for the speed upgrade (10/100/1000/10000/...)
        this.speedCtrl *= 10

        // reset timeout
        clearTimeout(this.timer)
        this.work()
    }


    /**
     * Unlock button access checker 
     */
    checkBtnsAccessibility() {
        // isf next DATA exist
        if (DATA[0].type[this.firm.productions.length]) {

            // if there are more than 10 workers in the production chain 
            if (this.firm.upgradeCondition <= this.firm.productions[this.firm.productions.length - 1].worker) {

                // change the css class and text of the unlock button
                this.firm.unlockButton.classList.toggle('desactivated', false)
                this.firm.unlockButton.innerHTML = "Débloque <span>" + (DATA[0].type[this.firm.productions.length].name).toUpperCase() + "</span> !!"

                // make the click accessible 
                this.firm.couldUpgrade = true
            }
        }
    }


    set duration(number = 10000) {
        this.productionTime = number
        // update delay values
        this.delay = this.productionTime
    }

}
