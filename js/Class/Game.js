import Society from './Society.js';
import Firm from './Firm.js';
import DATA from "../data.js";

// import Society from './Society.min.js';
// import Firm from './Firm.min.js';
// import DATA from "../data.min.js";

/**
 * Game class (homepage and start-game)
 */
export default class Game {

    /**
     * Game constructor
     * @param {HTMLElement} container 
     */
    constructor(container) {
        this.society = new Society(2000)
        this.firms = []
        this.ico = "🦔"
        this.title = "Farmer Town"
        this.container = container ? container : document.body
        this.anim = {}
        this.homePage()
    }

    /**
     * Draw the home page
     */
    homePage() {
        // Game container
        const game = document.createElement('div')
        game.id = '_game'

        // Title
        const title = document.createElement('h1')
        title.innerHTML = `<span class="ico-title">${this.ico}</span>`
        title.innerHTML += this.title

        // Start button
        const startBtn = document.createElement('button')
        startBtn.id = '_btn_start'
        startBtn.textContent = "Start Game !"

        // Text
        const text = document.createElement('div')
        text.innerHTML = "<h3>Adventure Agricole</h3>"
        text.innerHTML += "<p>Decouvre le monde agricole !</p>"

        // const div = document.createElement('div')
        // div.append(startBtn, text)

        // Append
        this.society.gameContainer.append(startBtn, this.society.gameBoxsContainer)
        game.append(title, this.society.gameContainer)
        this.container.appendChild(game)

        // Start Anim
        // this.startAnim()  

        // Start Button Listener
        startBtn.onclick = () => {
            clearInterval(this.anim)
            startBtn.classList.toggle('hidden', true)
            text.classList.toggle('hidden', true)
            title.innerHTML = "Farmer Town"
            this.society.gameContainer.classList.toggle('home', false)
            this.start();
        }
    }

    startAnim() {
        let i = 0
        this.anim = setInterval(() => {
            this.society.gameContainer.style.backgroundImage = `url("../images/anim/back${i}.png")`
            i >= 38 ? i = 0 : i++;
        }, 300)
    }

    /**
     * If startBtn is clicked, start the game !
     */
    start() {
        // header Game
        const head = this.society.drawHead()

        // get HTML-Elements for Society dom-controls and Start Birth event
        this.society.container = head
        this.society.birth()

        // Initialize a Firm 
        const firm = new Firm(
            this.society.gameContainer,
            this.society,
            DATA[0].domain,
        )

        // create a new production
        firm.newProductions(true)

        // push it into class propriety array
        this.firms.push(firm)

        // append unlock button
        this.society.gameContainer.append(this.firms[0].unlockButton)
    }

}