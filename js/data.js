const data = [
    {
        "domain": "Agricole",
        "type": [
            {
                "ico": "./images/icon/potager.png",
                "name": "potager",
                "cost": 20,
                "time": 2000,
                "gain": 7
            },
            {
                "ico": "./images/icon/verger.png",
                "name": "verger",
                "cost": 100,
                "time": 10000,
                "gain": 35
            },
            {
                "ico": "./images/icon/elevage-caprin.png",
                "name": "elevage caprin",
                "cost": 500,
                "time": 30000,
                "gain": 175
            },
            {
                "ico": "./images/icon/elevage-bovin.png",
                "name": "elevage bovin",
                "cost": 1000,
                "time": 50000,
                "gain": 350
            },
            {
                "ico": "./images/icon/vigne.png",
                "name": "vignoble",
                "cost": 10000,
                "time": 75000,
                "gain": 3500
            },
            {
                "ico": "./images/icon/vigne-plus.png",
                "name": "vignoble AOC",
                "cost": 25000,
                "time": 150000,
                "gain": 8350
            },
            {
                "ico": "./images/icon/domain.png",
                "name": "domaine",
                "cost": 75000,
                "time": 300000,
                "gain": 25000
            },
            {
                "ico": "./images/icon/domain-plus.png",
                "name": "domaine AOC",
                "cost": 150000,
                "time": 500000,
                "gain": 50000
            },
            {
                "ico": "./images/icon/ranch.png",
                "name": "ranch polyculture",
                "cost": 500000,
                "time": 500000,
                "gain": 175000
            },
            {
                "ico": "./images/icon/ranch-plus.png",
                "name": "ranch polyculture AOC USA",
                "cost": 1000000,
                "time": 1000000,
                "gain": 350000
            }
        ]
    }
]


export default data;