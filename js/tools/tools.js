/**
 * Create Id with random letters and numbers
 * @returns {string} Random string concatenation Ex. "8e96_d77b_aae4_e6fa"
 */
function uniqueId() {
    const id = () => {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return id() + "_" + id() + "_" + id() + "_" + id()
}


/**
 * Create a HTML Element.
 * This is to facilitate and minimize the insertion of HTML code 
 * @param {string} type HtmlElements type (div, span, p ...).
 * @param {string} name Class name for the element, place '-' between words 
 * @param {boolean} id If true the name is also used for the id with the '-' replaced by '_'
 * @param {string} text the text content of the element
 * @param {string} dataSet the dataset of the element
 * @returns 
 */
function createElements(type = "div", name = "", text = "", hasId = false, dataSet = "") {

    // Create HTMLElement  
    const element = document.createElement(type)

    // Add Values
    element.className = name
    if (hasId) { element.id = name.replace("-", "_") }
    if (text !== "") { element.textContent = text }
    if (dataSet !== "") { element.dataset.box_name = dataSet }

    // return HTMLElement  
    return element;
}


/**
 * Used to get model from data.json
 * @param {string} file Path of the file
 * @param {function} callback Callback answer
 */
function getModel(file, callback) {
    const rawFile = new XMLHttpRequest();
    rawFile.overrideMimeType("application/json");
    rawFile.open("GET", file, true);
    rawFile.onreadystatechange = function () {
        if (rawFile.readyState === 4 && rawFile.status == "200") {
            callback(rawFile.responseText);
        }
    }
    rawFile.send(null);
}


/**
 * Format big digit to local number
 * (Max output model is billions | B)
 * @param {number} number 
 * @returns number formatted 
 */
function roundNumber(number) {
    return new Intl.NumberFormat('fr-FR', {
        notation: "compact",
        compactDisplay: "short" // | long
    }).format(number);
}


/**
 * Adapt size of text to screen. (For production Type names)
 * Base size = 1.75rem  
 * Max word length = 10  
 * @param {string} text 
 * @returns {string} Size of text in 'rem'
 */
function adaptiveFontSize(text) {
    const txt = text.trim().split('').length
    if (10 < txt && 15 >= txt) { return "1.5rem" }
    else if (txt > 15) { return "1.25rem" }
    else { return "1.75rem" }
}

export { uniqueId, createElements, getModel, roundNumber, adaptiveFontSize };